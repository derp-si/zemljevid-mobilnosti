#!/bin/bash

set -eou

TMPFOLDER=$(mktemp -d)

mkdir -p src/vendor
aria2c -i vendor.txt -d src/vendor/ --auto-file-renaming=false --allow-overwrite=true

mkdir -p src/assets/marker/optimized
cp src/assets/marker/*.svg "${TMPFOLDER}"

inkscape -w 32 -h 32 --export-type=png ${TMPFOLDER}/*.svg

cp ${TMPFOLDER}/*.png src/assets/marker/optimized/

inkscape -w 192 -h 192 --export-type=png src/assets/logo.svg
mv src/assets/logo.png src/assets/logo_192.png

inkscape -w 64 -h 64 --export-type=png src/assets/logo.svg
mv src/assets/logo.png src/assets/logo_64.png
