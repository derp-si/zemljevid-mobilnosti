const GOOGLE_API_KEY = "AIzaSyDdmz_fdXT1G84423IYIkgp5SxlCHKvUo8";

function normalize(text) {
    return text.toLowerCase();
}

async function search(text) {
    text = normalize(text);
    if (text.length < 1) return [];

    // Search locally
    let results = [];
    for (let provider of Object.values(PROVIDERS)) {
        if (!provider.visible) continue;
        for (let feature of map.getSource(provider.id)._data.features || []) {
            let title = feature.properties.title || feature.properties.name || "";
            if (normalize(title).indexOf(text) >= 0)
                results.push({
                    id: feature.id,
                    title: title,
                    provider: provider.name,
                    image: 'assets/marker/optimized/' + provider.id + '.png',
                    feature: feature.properties,
                    coordinates: feature.geometry.coordinates,
                })
        }
    }

    // Search Google Places
    let res = await fetch(`https://cors.proxy.prometko.si/https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${GOOGLE_API_KEY}&components=country:si&input=${text}`);
    let googleResults = await res.json();
    for (let prediction of googleResults.predictions) {
        results.push({
            id: prediction.place_id,
            title: prediction.structured_formatting.main_text,
            subtitle: prediction.structured_formatting.secondary_text,
            image: 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1NzYgNTEyIj48IS0tISBGb250IEF3ZXNvbWUgUHJvIDYuNC4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlIChDb21tZXJjaWFsIExpY2Vuc2UpIENvcHlyaWdodCAyMDIzIEZvbnRpY29ucywgSW5jLiAtLT48cGF0aCBkPSJNNDA4IDEyMGMwIDcwLTEyMCAyMDAtMTIwIDIwMHMtMTIwLTEzMC0xMjAtMjAwQzE2OCA1My43IDIyMS43IDAgMjg4IDBzMTIwIDUzLjcgMTIwIDEyMHpNMTYwIDIwMC45VjQ0OEwwIDUxMlYxOTJsMTMzLTUzLjJjNS41IDE5LjQgMTUuMSA0MC42IDI3IDYyLjF6bTIyNCA1MS41VjUxMkwxOTIgNDQ4VjI1Mi4zYzQ0LjUgNjUuMyA5NiAxMjEgOTYgMTIxczUxLjQtNTUuNyA5Ni0xMjF6bTMyLTUxLjVjMi4xLTMuOCA0LjEtNy41IDYtMTEuM0w1NzYgMTI4VjQ0OEw0MTYgNTEyVjIwMC45ek0yODggMTUyYTQwIDQwIDAgMSAwIDAtODAgNDAgNDAgMCAxIDAgMCA4MHoiLz48L3N2Zz4=',
            provider: '_googlePlaces',
            feature: prediction,
        })
    }

    return results;
}

async function onSearch(field) {
    let nodes = [...field.parentElement.parentElement.children]
    let index = nodes.indexOf(field.parentElement);

    let results = await search(field.value.trim());

    let $_results = "";
    for (let result of results) {
        $_results += html`
		<div class="search-result" data-provider="${result.provider}" data-id="${result.id}" data-title="${result.title}" data-coordinates="${result.coordinates}">
			<strong>${result.title}</strong><br>
			<small>${result.provider && !result.subtitle ? `<img style="height:.75rem" src="${result.image}" /> ${result.provider}` : `<img style="height:.75rem" src="${result.image}" /> ${result.subtitle}`}</small>
		</div>`
    }

    if (results.length < 1)
        hideInfoPanel(1)
    else
        showInfoPanel(1, `<div id="search-results">${$_results}</div>`);

    // document.getElementById("search-results").innerHTML = $_results;
    //document.getElementById("search-results").dataset.searchIndex = index;

    // Add click handlers
    for (let $result of document.getElementsByClassName("search-result")) {

        // Called when a search result is clicked
        let _callback = async e => {
            console.debug(index)

            // Move data to search field
            let $search = document.getElementById("search-fields").children[index];
            $search.getElementsByTagName("input")[0].value = $result.dataset.title;
            $search.dataset.provider = $result.dataset.provider;
            $search.dataset.id = $result.dataset.id;
            $search.dataset.latLngString = $result.dataset.coordinates.split(",").reverse().join(",");
            map.flyTo({
                center: $result.dataset.coordinates.split(",").map(x => parseFloat(x)),
                essential: true,
                zoom: 16
            });
            // Clear search field and add another
            document.getElementById("search-results").innerHTML = "";
            // if there are already 2 search-bar classes on page, don't add another
            if (document.getElementsByClassName("search-bar").length < 2) {
                let $field = addSearchAfter($search);
                $field.querySelector("input").focus();
            }

            updateNavigation();
        };
        let callback = _callback;

        // Special case for Google Places, because coordinates are not included
        if ($result.dataset.provider == '_googlePlaces') {
            console.debug("googl");
            callback = async e => {
                let r = await fetch(`https://cors.proxy.prometko.si/https://maps.googleapis.com/maps/api/place/details/json?key=${GOOGLE_API_KEY}&fields=geometry&place_id=${$result.dataset.id}`)
                let place = await r.json();
                console.debug(place, place)
                $result.dataset.coordinates = [place.result.geometry.location.lng, place.result.geometry.location.lat];
                return await _callback(e);
            }
        }

        console.debug(callback);

        $result.addEventListener('click', callback);
    }

}

function searchField() {
    let $searchBar = document.createElement("div");
    $searchBar.classList.add("search-bar");
    $searchBar.innerHTML = html`
		<span class="drag-handle">⣶</span>
		<input type="text" placeholder="Iskanje" oninput="debounce(onSearch)(this)">
		<button class="clear-search-btn" onclick="deleteSearchField(this.parentElement)">×</button>
		<button class="add-search-btn" onclick="addSearchAfter(this.parentElement)">+</button>
	`;
    return $searchBar;
}

function deleteSearchField($searchBar) {
    $searchBar.remove();
    document.querySelector(".search-bar input").value = "";
    updateNavigation();
}

function addSearchAfter($searchBar) {
    let count = document.getElementById("search-fields").children.length;
    if (count > 1) return;
    let f = searchField();
    insertAfter(f, $searchBar)
    return f;
}

let travel_types = {
    "TRANSIT,WALK"             : "Javni prevoz",
    "BUS,WALK"                 : "Avtobus",
    "TRAM,RAIL,SUBWAY,FUNICULAR,GONDOLA,WALK": "Vlak",
    "WALK"                     : 'Hoja',
    'BICYCLE_RENT'             : 'Izposoja kolesa',
    'CAR_RENT'                 : 'Izposoja avtomobila',
    'TRANSIT,BICYCLE_RENT'     : 'Javni prevoz in izposoja kolesa',
    'TRANSIT,CAR_RENT'         : 'Javni prevoz in izposoja avtomobila',

}


function updateNavigation() {
    let list = [];
    for (let $search of document.getElementsByClassName("search-bar"))
        list.push($search.dataset.latLngString);

    // TODO: this whole function needs to be reworked to support intermediate points

    if (list[0] == undefined || list[1] == undefined)
        return;

    if (list.length < 2) {
        hideInfoPanel(1);
        document.getElementById("nav-options").hidden = true;
        return;
    } else {
        document.getElementById("nav-options").hidden = false;
    }

    let departure_time = document.getElementById("departure-time").value;
    let arrival_time = document.getElementById("arrival-time").value.replaceAll(':', '%3A')
    let transit_type = document.getElementById("transit-type").value.replaceAll(',', '%2C');

    let url = "https://otp.ojpp.derp.si/otp/routers/default/plan?";
    let params = [];
    params.push(`fromPlace=${list[0]}&toPlace=${list[1]}`);
    params.push(`mode=${transit_type}`);
    
    if (arrival_time.length > 0) 
        params.push(`arriveBy=true&time=${arrival_time}`);
    else if (departure_time.length > 0) 
        params.push(`time=${departure_time}`);
    params.push(`date=${new Date().toISOString().slice(0, 10)}`);

    url += params.join("&");
    NavigationState.url = url;

    showInfoPanel(1, $_SPINNER);
    fetch(url).then(e => e.json()).then(displayNavigation);
}

window.NavigationState = {
    itineraries: [],
    url: "",
    nextPageUrl: "",
}

function nextOtpUrl(response) {
    return NavigationState.url + "&pageCursor=" + response.nextPageCursor;
}

function displayNavigation(response) {
    if (response.error) {
        console.error("OTP ERROR", response.error);

        let msg = "Prišlo je do napake";
        if (response.error.message == "PATH_NOT_FOUND")
            msg = renderItineraries({plan:{itineraries:[]}}, 0)

        showInfoPanel(1, html`<div class="error">${msg}</div>`);
        return;
    }
    NavigationState.itineraries = response.plan.itineraries;
    NavigationState.nextPageUrl = nextOtpUrl(response);
    let $_nagivation = html`
    <div id="itineraties">
        ${renderItineraries(response)}
    </div>
    <button onclick="loadMoreItineraries(this)" class="load-more-itineraries">Naloži več</button>
    `;
    showInfoPanel(1, $_nagivation)
    onItineraryClick(0);
}

function loadMoreItineraries(btn) {
    btn.disabled = true;
    fetch(NavigationState.nextPageUrl).then(e => e.json()).then(response => {
        btn.disabled = false;
        
        document.getElementById("itineraties").innerHTML += renderItineraries(response, NavigationState.itineraries.length);
        
        NavigationState.nextPageUrl = nextOtpUrl(response);
        NavigationState.itineraries.push(...response.plan.itineraries);
    });
}

function formatMeters(m) {
    m = Math.ceil(m)
    if (m < 1000)
        return `${m}&nbsp;m`;
    else
        return `${(m / 1000).toFixed(2)}&nbsp;km`.replace('.', ',');
}

function formatDuration(s) {
    s = Math.ceil(s / 60) * 60;
    let duration = {
        h: Math.floor(s / 3600),
        m: Math.floor(s / 60) % 60,
        s: s % 60,
    }
    return `${duration.h > 0 ? duration.h + "&nbsp;h " : ""}${duration.m > 0 ? duration.m + "&nbsp;min " : ""}${duration.s > 0 ? duration.s + "&nbsp;s" : ""}`.trim()
}

// Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc.
let ICONS = {
    "BUS": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path d="M288 0C422.4 0 512 35.2 512 80V96l0 32c17.7 0 32 14.3 32 32v64c0 17.7-14.3 32-32 32l0 160c0 17.7-14.3 32-32 32v32c0 17.7-14.3 32-32 32H416c-17.7 0-32-14.3-32-32V448H192v32c0 17.7-14.3 32-32 32H128c-17.7 0-32-14.3-32-32l0-32c-17.7 0-32-14.3-32-32l0-160c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h0V96h0V80C64 35.2 153.6 0 288 0zM128 160v96c0 17.7 14.3 32 32 32H272V128H160c-17.7 0-32 14.3-32 32zM304 288H416c17.7 0 32-14.3 32-32V160c0-17.7-14.3-32-32-32H304V288zM144 400a32 32 0 1 0 0-64 32 32 0 1 0 0 64zm288 0a32 32 0 1 0 0-64 32 32 0 1 0 0 64zM384 80c0-8.8-7.2-16-16-16H208c-8.8 0-16 7.2-16 16s7.2 16 16 16H368c8.8 0 16-7.2 16-16z"/></svg>`,
    "RAIL": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M96 0C43 0 0 43 0 96V352c0 48 35.2 87.7 81.1 94.9l-46 46C28.1 499.9 33.1 512 43 512H82.7c8.5 0 16.6-3.4 22.6-9.4L160 448H288l54.6 54.6c6 6 14.1 9.4 22.6 9.4H405c10 0 15-12.1 7.9-19.1l-46-46c46-7.1 81.1-46.9 81.1-94.9V96c0-53-43-96-96-96H96zM64 96c0-17.7 14.3-32 32-32H352c17.7 0 32 14.3 32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V96zM224 288a48 48 0 1 1 0 96 48 48 0 1 1 0-96z"/></svg>`,
    "FERRY": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><defs><style>.fa-secondary{opacity:.4}</style></defs><path class="fa-primary" d="M224 0c-17.7 0-32 14.3-32 32V64H144c-26.5 0-48 21.5-48 48V240L51.6 254.8c-23.1 7.7-29.5 37.5-11.5 53.9l101 92.6c16.2 9.4 34.7 15.1 50.9 15.1c21.1 0 42-8.5 59.2-20.3c22.1-15.5 51.6-15.5 73.7 0c18.4 12.7 39.6 20.3 59.2 20.3c16.2 0 34.7-5.7 50.9-15.1l101-92.6c18-16.5 11.6-46.2-11.5-53.9L480 240V112c0-26.5-21.5-48-48-48H384V32c0-17.7-14.3-32-32-32H224zm43.8 182.7L160 218.7V128H416v90.7L308.2 182.7c-13.1-4.4-27.3-4.4-40.5 0z"/><path class="fa-secondary" d="M269.5 421.9c11.1-7.9 25.9-7.9 37 0C329 437.4 356.5 448 384 448c26.9 0 55.3-10.8 77.4-26.1l0 0c11.9-8.5 28.1-7.8 39.2 1.7c14.4 11.9 32.5 21 50.6 25.2c17.2 4 27.9 21.2 23.9 38.4s-21.2 27.9-38.4 23.9c-24.5-5.7-44.9-16.5-58.2-25C449.5 501.7 417 512 384 512c-31.9 0-60.6-9.9-80.4-18.9c-5.8-2.7-11.1-5.3-15.6-7.7c-4.5 2.4-9.7 5.1-15.6 7.7c-19.8 9-48.5 18.9-80.4 18.9c-33 0-65.5-10.3-94.5-25.8c-13.4 8.4-33.7 19.3-58.2 25c-17.2 4-34.4-6.7-38.4-23.9s6.7-34.4 23.9-38.4c18.1-4.2 36.2-13.3 50.6-25.2c11.1-9.4 27.3-10.1 39.2-1.7l0 0C136.7 437.2 165.1 448 192 448c27.5 0 55-10.6 77.5-26.1z"/></svg>`,
    "CABLE_CAR": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M288 0a32 32 0 1 1 0 64 32 32 0 1 1 0-64zM160 56a32 32 0 1 1 64 0 32 32 0 1 1 -64 0zM32 288c0-35.3 28.7-64 64-64H232V157.5l-203.1 42c-13 2.7-25.7-5.7-28.4-18.6s5.7-25.7 18.6-28.4l232-48 232-48c13-2.7 25.7 5.7 28.4 18.6s-5.7 25.7-18.6 28.4L280 147.5V224H416c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H96c-35.3 0-64-28.7-64-64V288zm64 0c-8.8 0-16 7.2-16 16v64c0 8.8 7.2 16 16 16h64c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16H96zm112 16v64c0 8.8 7.2 16 16 16h64c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16H224c-8.8 0-16 7.2-16 16zm144-16c-8.8 0-16 7.2-16 16v64c0 8.8 7.2 16 16 16h64c8.8 0 16-7.2 16-16V304c0-8.8-7.2-16-16-16H352z"/></svg>`,
    "WALK": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M160 48a48 48 0 1 1 96 0 48 48 0 1 1 -96 0zM126.5 199.3c-1 .4-1.9 .8-2.9 1.2l-8 3.5c-16.4 7.3-29 21.2-34.7 38.2l-2.6 7.8c-5.6 16.8-23.7 25.8-40.5 20.2s-25.8-23.7-20.2-40.5l2.6-7.8c11.4-34.1 36.6-61.9 69.4-76.5l8-3.5c20.8-9.2 43.3-14 66.1-14c44.6 0 84.8 26.8 101.9 67.9L281 232.7l21.4 10.7c15.8 7.9 22.2 27.1 14.3 42.9s-27.1 22.2-42.9 14.3L247 287.3c-10.3-5.2-18.4-13.8-22.8-24.5l-9.6-23-19.3 65.5 49.5 54c5.4 5.9 9.2 13 11.2 20.8l23 92.1c4.3 17.1-6.1 34.5-23.3 38.8s-34.5-6.1-38.8-23.3l-22-88.1-70.7-77.1c-14.8-16.1-20.3-38.6-14.7-59.7l16.9-63.5zM68.7 398l25-62.4c2.1 3 4.5 5.8 7 8.6l40.7 44.4-14.5 36.2c-2.4 6-6 11.5-10.6 16.1L54.6 502.6c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3L68.7 398z"/></svg>`,
    "BICYCLE": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M312 32c-13.3 0-24 10.7-24 24s10.7 24 24 24h25.7l34.6 64H222.9l-27.4-38C191 99.7 183.7 96 176 96H120c-13.3 0-24 10.7-24 24s10.7 24 24 24h43.7l22.1 30.7-26.6 53.1c-10-2.5-20.5-3.8-31.2-3.8C57.3 224 0 281.3 0 352s57.3 128 128 128c65.3 0 119.1-48.9 127-112h49c8.5 0 16.3-4.5 20.7-11.8l84.8-143.5 21.7 40.1C402.4 276.3 384 312 384 352c0 70.7 57.3 128 128 128s128-57.3 128-128s-57.3-128-128-128c-13.5 0-26.5 2.1-38.7 6L375.4 48.8C369.8 38.4 359 32 347.2 32H312zM458.6 303.7l32.3 59.7c6.3 11.7 20.9 16 32.5 9.7s16-20.9 9.7-32.5l-32.3-59.7c3.6-.6 7.4-.9 11.2-.9c39.8 0 72 32.2 72 72s-32.2 72-72 72s-72-32.2-72-72c0-18.6 7-35.5 18.6-48.3zM133.2 368h65c-7.3 32.1-36 56-70.2 56c-39.8 0-72-32.2-72-72s32.2-72 72-72c1.7 0 3.4 .1 5.1 .2l-24.2 48.5c-9 18.1 4.1 39.4 24.3 39.4zm33.7-48l50.7-101.3 72.9 101.2-.1 .1H166.8zm90.6-128H365.9L317 274.8 257.4 192z"/></svg>`,
    "CAR": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M135.2 117.4L109.1 192H402.9l-26.1-74.6C372.3 104.6 360.2 96 346.6 96H165.4c-13.6 0-25.7 8.6-30.2 21.4zM39.6 196.8L74.8 96.3C88.3 57.8 124.6 32 165.4 32H346.6c40.8 0 77.1 25.8 90.6 64.3l35.2 100.5c23.2 9.6 39.6 32.5 39.6 59.2V400v48c0 17.7-14.3 32-32 32H448c-17.7 0-32-14.3-32-32V400H96v48c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32V400 256c0-26.7 16.4-49.6 39.6-59.2zM128 288a32 32 0 1 0 -64 0 32 32 0 1 0 64 0zm288 32a32 32 0 1 0 0-64 32 32 0 1 0 0 64z"/></svg>`,
    "WAIT": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M464 256A208 208 0 1 1 48 256a208 208 0 1 1 416 0zM0 256a256 256 0 1 0 512 0A256 256 0 1 0 0 256zM232 120V256c0 8 4 15.5 10.7 20l96 64c11 7.4 25.9 4.4 33.3-6.7s4.4-25.9-6.7-33.3L280 243.2V120c0-13.3-10.7-24-24-24s-24 10.7-24 24z"/></svg>`,
    "FINISH": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M32 0C49.7 0 64 14.3 64 32V48l69-17.2c38.1-9.5 78.3-5.1 113.5 12.5c46.3 23.2 100.8 23.2 147.1 0l9.6-4.8C423.8 28.1 448 43.1 448 66.1V345.8c0 13.3-8.3 25.3-20.8 30l-34.7 13c-46.2 17.3-97.6 14.6-141.7-7.4c-37.9-19-81.3-23.7-122.5-13.4L64 384v96c0 17.7-14.3 32-32 32s-32-14.3-32-32V400 334 64 32C0 14.3 14.3 0 32 0zM64 187.1l64-13.9v65.5L64 252.6V318l48.8-12.2c5.1-1.3 10.1-2.4 15.2-3.3V238.7l38.9-8.4c8.3-1.8 16.7-2.5 25.1-2.1l0-64c13.6 .4 27.2 2.6 40.4 6.4l23.6 6.9v66.7l-41.7-12.3c-7.3-2.1-14.8-3.4-22.3-3.8v71.4c21.8 1.9 43.3 6.7 64 14.4V244.2l22.7 6.7c13.5 4 27.3 6.4 41.3 7.4V194c-7.8-.8-15.6-2.3-23.2-4.5l-40.8-12v-62c-13-3.8-25.8-8.8-38.2-15c-8.2-4.1-16.9-7-25.8-8.8v72.4c-13-.4-26 .8-38.7 3.6L128 173.2V98L64 114v73.1zM320 335.7c16.8 1.5 33.9-.7 50-6.8l14-5.2V251.9l-7.9 1.8c-18.4 4.3-37.3 5.7-56.1 4.5v77.4zm64-149.4V115.4c-20.9 6.1-42.4 9.1-64 9.1V194c13.9 1.4 28 .5 41.7-2.6l22.3-5.2z"/></svg>`,
    "START": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M48 24C48 10.7 37.3 0 24 0S0 10.7 0 24V64 350.5 400v88c0 13.3 10.7 24 24 24s24-10.7 24-24V388l80.3-20.1c41.1-10.3 84.6-5.5 122.5 13.4c44.2 22.1 95.5 24.8 141.7 7.4l34.7-13c12.5-4.7 20.8-16.6 20.8-30V66.1c0-23-24.2-38-44.8-27.7l-9.6 4.8c-46.3 23.2-100.8 23.2-147.1 0c-35.1-17.6-75.4-22-113.5-12.5L48 52V24zm0 77.5l96.6-24.2c27-6.7 55.5-3.6 80.4 8.8c54.9 27.4 118.7 29.7 175 6.8V334.7l-24.4 9.1c-33.7 12.6-71.2 10.7-103.4-5.4c-48.2-24.1-103.3-30.1-155.6-17.1L48 338.5v-237z"/></svg>`,
    "ERROR": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!--! Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M256 32c14.2 0 27.3 7.5 34.5 19.8l216 368c7.3 12.4 7.3 27.7 .2 40.1S486.3 480 472 480H40c-14.3 0-27.6-7.7-34.7-20.1s-7-27.8 .2-40.1l216-368C228.7 39.5 241.8 32 256 32zm0 128c-13.3 0-24 10.7-24 24V296c0 13.3 10.7 24 24 24s24-10.7 24-24V184c0-13.3-10.7-24-24-24zm32 224a32 32 0 1 0 -64 0 32 32 0 1 0 64 0z"/></svg>`,
}

let line_colors = { "1": "C93336", "2": "8C8841", "3": "EC593A", "5": "9F539E", "6": "939598", "7": "1CBADC", "8": "116AB0", "9": "86AACD", "11": "EDC23B", "12": "214AA0", "13": "CFD34D", "14": "EF59A1", "15": "A2238E", "18": "895735", "19": "EA9EB4", "20": "1F8751", "21": "52BA50", "22": "F6A73A", "24": "ED028C", "25": "0F95CA", "26": "231F20", "27": "57A897", "30": "9AD2AE", "40": "496E6D", "42": "A78B6B", "43": "4E497A", "44": "817EA8", "51": "6C8BC6", "52": "00565D", "53": "C7B3CA", "56": "953312", "60": "ACBB71", "61": "F9A64A", "71": "6C8BC6", "72": "4CA391", "73": "FECA0A", "78": "C96D6A", "16": "582C81", "23": "40AE49", };

const NETWORK_MAP = {
    'jcdecaux_maribor': 'mbajk',
    'jcdecaux_ljubljana': 'bicikelj',

}

function network_map(network) {
    if (network.includes('nomago') || network.includes('nextbike')) {
        return 'nomago_bikes';
    }
    if (network.includes('scbikes')) {
        return 'scbikes';
    }
    return NETWORK_MAP[network] || network;
}

function sharing_chip(leg, from_to) {
    let icon = "";
    if (leg[from_to].networks) {
        icon = html`<img src="assets/marker/optimized/${network_map(leg[from_to].networks[0])}.png">`
    }
    return html`<span class="sharing-chip">
        ${icon}
        <span class="sharing-chip-text">${leg[from_to].name}</span>
    </span>`
}

function renderItineraries(response, index = 0) {
    let $_navigation = "";
    if (response.plan.itineraries.length == 0) {
        $_navigation = html`<div class="itinerary-navigation">
            <div class="itinerary-navigation-item">
            <span class="itinerary-entry">${ICONS['ERROR']}</span>
                <div class="itinerary-navigation-item-text">Ni najdenih poti.</div>
            </div>
        </div>`;
    }
    for (let itinerary of response.plan.itineraries) {
        
        let legs = []
        let steps = []
        let last_time = itinerary.startTime;
        
        for (let leg of itinerary.legs) {
            let $_wait = "";
            if (((leg.startTime - last_time) / 1000) > 300) {
                let time = (leg.startTime - last_time) / 1000;
                $_wait = `<span class="itinerary-entry">${ICONS['WAIT']}&nbsp;<span class="itinerary-entry-text">${formatDuration(time)}</span></span>`;
                legs.push($_wait);
                steps.push(`<div class="itinerary-step">Počakaj ${formatDuration(time)}.</div>`)
            }
            switch (leg.mode) {
                case "WALK":
                    if (leg.distance < 50) continue;
                    legs.push(`<span class="itinerary-entry">${ICONS['WALK']}&nbsp;<span class="itinerary-entry-text">${formatDuration(leg.duration)} | ${formatMeters(leg.distance)}</span></span>`);
                    steps.push(`<div class="itinerary-step">Hodi od ${leg.from.name} do ${leg.to.name} (${formatDuration(leg.duration)})</div>`)
                    break;
                case "BUS":
                    legs.push(`<span class="itinerary-entry"><span class="bus-wrapper ${leg.agencyId == "2:lpp" ? "inverse" : ""}" ${leg.agencyId == "2:lpp" ? `style="background-color:#${line_colors[leg.routeShortName.replaceAll(/[^\d-]/g, "")] || "aaa"}; color:white;"` : ""}>${ICONS['BUS']}<span>${leg.routeShortName || leg.to.name}</span></span><span>&nbsp;</span><span class="itinerary-entry-text">${formatDuration(leg.duration)}</span></span>`);
                    steps.push(`<div class="itinerary-step">S postaje ${leg.from.name} se pelji z avtobusom ${leg.routeShortName || leg.routeName} do postaje ${leg.to.name} (${formatDuration(leg.duration)})</div>`)
                    break;
                case "RAIL":
                    legs.push(`<span class="itinerary-entry"><span class="bus-wrapper">${ICONS['RAIL']}<span>&nbsp;</span><span>${leg.routeShortName || leg.to.name}</span></span><span>&nbsp;</span><span class="itinerary-entry-text">${formatDuration(leg.duration)}</span></span>`);
                    steps.push(`<div class="itinerary-step">S postaje ${leg.from.name} se pelji z vlakom ${leg.routeShortName || leg.routeName} do postaje ${leg.to.name} (${formatDuration(leg.duration)})</div>`)
                    break;
                case "FERRY":
                    legs.push(`<span class="itinerary-entry"><span class="bus-wrapper">${ICONS['FERRY']}<span>&nbsp;</span><span>${leg.routeShortName || leg.route}</span></span><span>&nbsp;</span><span class="itinerary-entry-text">${formatDuration(leg.duration)}</span></span>`);
                    steps.push(`<div class="itinerary-step">S postaje ${leg.from.name} se pelji z ladjo ${leg.routeShortName || leg.routeName} do postaje ${leg.to.name} (${formatDuration(leg.duration)})</div>`)
                    break;
                case "BICYCLE":
                    /* like walk but with a bike icon */
                    legs.push(`<span class="itinerary-entry">${ICONS['BICYCLE']}&nbsp;<span class="itinerary-entry-text">${formatDuration(leg.duration)} | ${formatMeters(leg.distance)}</span></span>`);
                    steps.push(`<div class="itinerary-step">Kolesari od ${sharing_chip(leg, 'from')}</span> do ${sharing_chip(leg, 'to')}</span> (${formatDuration(leg.duration)}).</div>`)
                    break;
                case "CAR": /*TODO: ???*/
                    legs.push(`<span class="itinerary-entry">${ICONS['CAR']}&nbsp;<span class="itinerary-entry-text">${formatDuration(leg.duration)} | ${formatMeters(leg.distance)}</span></span>`);
                    steps.push(`<div class="itinerary-step">Pelji se od ${sharing_chip(leg, 'from')} do ${sharing_chip(leg, 'to')} (${formatDuration(leg.duration)})</div>`)
                    break;
                default:
                    legs.push(`<span class="itinerary-entry">${ICONS['WALK']}&nbsp;<span class="itinerary-entry-text">${formatDuration(leg.duration)} | ${formatMeters(leg.distance)}</span></span>`);
                    steps.push(`<div class="itinerary-step">Pojdi od ${leg.from.name} do ${leg.to.name} (${formatDuration(leg.duration)})</div>`)
                    break;  
            }
            last_time = leg.endTime
        }
        // legs.push(`<span class="itinerary-entry">${ICONS["FINISH"]}</span>`)
        let $_itinerary = html`
        <div class="itinerary" data-itinerary-index="${index}" onclick="onItineraryClick(parseInt(this.dataset.itineraryIndex))">
            <div class="legs">
                ${legs.join('<span class="leg-join-icon">&nbsp;>&nbsp;</span>')}
            </div>
            <div class="times">
                <div class="itinerary-entry">
                    ${ICONS["START"]}
                    &nbsp;
                    ${formatTime(new Date(itinerary.startTime))}
                </div>
                <div class="times-line"></div>
                <div class="itinerary-entry">
                    ${ICONS["WAIT"]}
                    &nbsp;
                    ${formatDuration(itinerary.duration)}
                </div>
                <div class="times-line"></div>
                <div class="itinerary-entry"> 
                    ${ICONS["FINISH"]}
                    &nbsp;
                    ${formatTime(new Date(itinerary.endTime))}
                </div>
            </div>
            <div class="legs-full">
                ${steps.join('')}
            </div>
            </div>
        </div>`;
        $_navigation += $_itinerary;

        index++;
    }
    return $_navigation;
}

function onItineraryClick(index) {
    let itinerary = NavigationState.itineraries[index];
    // TODO: is this needed?
    if (!itinerary)
        return;
    
    let $el = document.querySelector('.itinerary.expanded');
    if ($el)
        $el.classList.remove('expanded');

    $el = document.querySelector(`.itinerary[data-itinerary-index="${index}"]`);
    $el.classList.add('expanded');

    let geojson = itinerary_to_geojson(itinerary);
    map.getSource("lines").setData(geojson);

    // showItineraryPanel(itinerary)

    zoomToGeoJSON(geojson);
}

let MODE_COLORS = {
    'BUS': 'rgba(255, 50, 50, .9)',
    'RAIL': 'rgba(100, 200, 255, .9)',
    'WALK': 'rgba(150, 150, 150, .9)',
    'BICYCLE': 'rgba(100, 200, 100, .9)',
    'CAR': 'rgba(50, 0, 255, .9)',
}

function itinerary_to_geojson(itinerary) {
    let collection = {
        'type': 'FeatureCollection',
        'features': []
    };
    for (let leg of itinerary.legs) {
        collection.features.push({
            'type': 'Feature',
            'properties': {
                'color': MODE_COLORS[leg.mode] || "black"
            },
            'geometry': polyline.toGeoJSON(leg.legGeometry.points)
        });
    }
    return collection;
}

function initSearch() {
    document.getElementById("search-fields").appendChild(searchField());

}

function createMarker(markerNum, latLng) {
    let $el = document.createElement("div");
    $el.innerHTML = html`
    <div class="placed-marker">
        ${markerNum}
    </div>`;

    let marker = new maplibregl.Marker($el, {
        draggable: true,

    })
    .setLngLat(latLng)
    .addTo(map);

    marker.on('dragend', onMarkerDragEnd);

    return marker;
}