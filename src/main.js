window.USE_BOTTOM_SHEET = window.innerWidth < window.innerHeight;

$provider_toggles = document.getElementById("provider-toggles");

function darkMode(dark) {
	if (dark) {
		document.body.classList.add("dark");
		document.getElementById("themeDark").checked = true;
	} else {
		document.body.classList.remove("dark");
		document.getElementById("themeLight").checked = true;
	}
}

darkMode(window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches);
window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', e => {
	darkMode(e.matches);
});

let UI_HIDDEN = false;

function toggleUI(toggle = undefined) {
	if (toggle === undefined)
		toggle = !UI_HIDDEN;
	else if (toggle == UI_HIDDEN)
		return;

	UI_HIDDEN = toggle;
	let timeout = parseFloat(getComputedStyle(document.body).getPropertyValue("--ui-hide-time")) * 1000;

	if (UI_HIDDEN) {
		document.body.classList.add("ui-hiding");
		setTimeout(() => {
			document.body.classList.add("ui-hidden");
		}, timeout);
	} else {
		document.body.classList.remove("ui-hidden");
		document.body.classList.remove("ui-hiding");
	}
	
}


function initSidebar() {
	let $sidebar = document.getElementById("providers");
	let $_sidebar = `<div>`;

	for (let section of SECTIONS) {
		let $_section = html`
		<div class="provider-section">
			<div class="provider-section-header">${section.title}</div>
		`;

		let $_group = "";
		for (let group of section.provider_groups) {
			$_group += html`
			<span class="provider-group-toggle" onclick="groupSetVisible(this.dataset.providerGroup)" data-provider-group='${group.name}'>
				<img src="assets/marker/optimized/${group.name}.png" alt="${group.name}" title="${group.name}"> 
			</span>
			`;
		}
		$_section += $_group;
		$_section += html`</div>`;

		$_sidebar += $_section;
	}
	$_sidebar += html`</div>`;

	$sidebar.innerHTML = $_sidebar;
}

initSidebar()

function infoPanelTemplate(content) {
	let $_panel = ``;

	if (content.image) {
		$_panel += html`<div class="info-panel-img"><img src="${imgProxy(content.image.src, 1080)}"></div>`
	}

	if (content.title)
		$_panel += html`<h2>${content.title}</h2>`;

	if (content.subtitle)
		$_panel += html`<p>${content.subtitle}</p>`;

	if (content.body)
		$_panel += content.body

	return $_panel;
}


initSearch();

window.currentPanel = 0;

if (window.USE_BOTTOM_SHEET) {

	// Disable desktop panels
	document.getElementById("panel-container").style.setProperty('display', 'none');
	document.getElementById("navigation-sidebar").style.setProperty('height', 'auto');


	window.addEventListener("popstate", (event) => {
		hideInfoPanel(window.currentPanel)
	});

	document.getElementById("sheet").addEventListener("sheet-close", () => {
		history.back();
	})

}

function showInfoPanel(layer, content) {
	if (window.USE_BOTTOM_SHEET) {
			
		let $infoPanel = document.querySelector("#sheet .body");
		if (content !== undefined) {
			if (typeof content != "string")
				content = infoPanelTemplate(content);

			$infoPanel.innerHTML = content;

			setSheetHeight(Math.min(50, 720 / window.innerHeight * 100))
			history.pushState({layer: layer}, "", "");
		}

		// setIsSheetShown(true)

		window.currentPanel = layer;
	} else {

		for (let i = layer+1; i <= 3; i++) {
			hideInfoPanel(i);
		}
		let $infoPanel = document.getElementById("info-panel-" + layer);
		if (content !== undefined) {
			if (typeof content != "string")
				content = infoPanelTemplate(content);

			$infoPanel.innerHTML = content;
		}
		$infoPanel.classList.remove("hidden");
		setTimeout(() => $infoPanel.classList.remove("hiding"), 100);

		window.currentPanel = layer;

	}
}

function hideInfoPanel(layer) {
	if (window.USE_BOTTOM_SHEET) {
			
	setSheetHeight(0);

	let $infoPanel = document.getElementById("info-panel-" + layer);
	$infoPanel.classList.add("hiding");
	setTimeout(() => {
		$infoPanel.classList.add("hidden");
		$infoPanel.classList.remove("hiding");
	}, 300);

	window.currentPanel = layer;

	} else {

		let $infoPanel = document.getElementById("info-panel-" + layer);
		$infoPanel.classList.add("hiding");
		setTimeout(() => {
			$infoPanel.classList.add("hidden");
			$infoPanel.classList.remove("hiding");
		}, 300);

		window.currentPanel = layer;

	}
}