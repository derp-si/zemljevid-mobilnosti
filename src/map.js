window.USE_VECTOR = (localStorage.getItem("USE_VECTOR") || "true") == "true";
if (USE_VECTOR)
	document.getElementById("renderingWebgl").checked = true;
else
	document.getElementById("renderingRaster").checked = true;

window.USE_ANTLINE = false;

let opts = {
	container: 'map',
	style: 'https://tiles.derp.si/maps/streets-v2/style.json',
	center: [14.505, 46.051],
	zoom: 11,
	minZoom: 7,
	renderWorldCopies: false,
	hash: true,
	maxBounds: [[10.2, 44.2], [19.4, 47.8]],
};

if (!USE_VECTOR) {
	opts.style = {
		'glyphs': "https://tiles.derp.si/fonts/{fontstack}/{range}.pbf",
		'version': 8,
		'sources': {
			'raster-tiles': {
				'type': 'raster',
				'tiles': [
					'https://tiles.derp.si/maps/streets/{z}/{x}/{y}@2x.png'
				],
				'tileSize': 256,
				'attribution': '',
			}
		},
		'layers': [
			{
				'id': 'simple-tiles',
				'type': 'raster',
				'source': 'raster-tiles',
				'minzoom': 0,
				'maxzoom': 21.5
			}
		]
	}
}
var map = new maplibregl.Map(opts);


var nav = new maplibregl.NavigationControl();
map.addControl(nav, 'top-right');

var scale = new maplibregl.ScaleControl({
	maxWidth: 80,
	unit: 'metric'
});
map.addControl(scale);

var popup = new maplibregl.Popup({
	closeButton: false,
	closeOnClick: false
});

let TRIED_IMAGES = [];

for (let provider_id in PROVIDERS) {
	let provider = PROVIDERS[provider_id];
	for (let img of provider.getImages()) {
		let url = `assets/marker/optimized/${img}.png`;
		TRIED_IMAGES.push(url);
		map.loadImage(url, function (error, image) {
			if (error) console.error("Error loading image", url, error)
			else if (!map.hasImage(img)) map.addImage(img, image);
		});
	}

}

let geolocate = new maplibregl.GeolocateControl({
	positionOptions: {
		enableHighAccuracy: true
	},
	trackUserLocation: true
})
map.addControl(geolocate);

geolocate.on('trackuserlocationend', function() {
	localStorage.setItem("hasGeoPermission", "true")
});


map.on('click', function(e) {
	// If no feature was clicked, toggle the UI
	let features = map.queryRenderedFeatures(e.point);
	for (let feature of features) 
		if (!feature.source.includes("maptiler_"))
			return;
	
	// toggleUI();
});

if (USE_VECTOR) {

	class Toggle3DControl {
		onAdd(map) {
			//this._map = map;
			this._container = document.createElement('div');
			this._container.className = 'maplibregl-ctrl maplibregl-ctrl-group';
			this._container.addEventListener('contextmenu', (e) => e.preventDefault());
			this._container.addEventListener('click', (e) => toggle3D());

			this._container.innerHTML = html`
		<div class="tools-box">
			<button>
				<span class="maplibregl-ctrl-icon" aria-hidden="true" title="Toggle 3D buildings" id="toggle-3d">3D</span>
			</button>
		</div>`;

			return this._container;
		}
	}
	map.addControl(new Toggle3DControl(), 'top-right');

}

class ToggleSidebar {
	onAdd(map) {
		//this._map = map;
		this._container = document.createElement('div');
		this._container.className = 'maplibregl-ctrl maplibregl-ctrl-group';
		this._container.addEventListener('contextmenu', (e) => e.preventDefault());
		this._container.addEventListener('click', (e) => sidebarjs.open());

		this._container.innerHTML = html`
	<div class="tools-box" id="open-sidebar">
		<button>
			<span class="maplibregl-ctrl-icon" aria-hidden="true" title="Open sidebar">☰</span>
		</button>
	</div>`;

		return this._container;
	}
}
map.addControl(new ToggleSidebar(), 'top-right');

map.on('load', function () {
	if (USE_VECTOR)
		toggle3D(localStorage.getItem("3d_buildings") == "false");

	loadErrorImage("error_image");

	if (localStorage.getItem("hasGeoPermission") == "true")
		geolocate.trigger();

	if (map.getLayer("Station"))
		map.setLayoutProperty("Station", "visibility", "none")

	if (map.getLayer("Other POI"))
		map.setFilter("Other POI", [
			"all",
			["!=", "class", "bus"],
			["!=", "class", "bicycle_rental"],
			["!=", "class", "railway"],
			["!=", "class", "bus"],
		])

	// Start loading provider asynchronously
	load_all().then(console.debug)

	//
	// Route line layer
	//

	map.addSource("lines", {
		"type": "geojson",
		"data": {
			"type": "Feature",
			"properties": {},
			"geometry": {
				"type": "LineString",
				"coordinates": [

				]
			}
		}
	})
	map.addLayer({
		"id": "lines_bottom",
		"type": "line",
		"source": 'lines',
		"paint": {
			"line-color": ['get', 'color'],
			"line-width": 8
		}
	});

	if (USE_ANTLINE) {
		map.addLayer({
			"id": "lines_top",
			"type": "line",
			"source": 'lines',
			"layout": {
				"line-join": "round",
				"line-cap": "butt"
			},
			"paint": {
				"line-color": "#888",
				"line-width": 6,
				// "line-opacity": 0.5,
			}
		});

		var dashLength = -1.5;
		var gapLength = -2;

		// We divide the animation up into 40 steps to make careful use of the finite space in
		// LineAtlas
		var steps = 40;
		// A # of steps proportional to the dashLength are devoted to manipulating the dash
		var dashSteps = steps * dashLength / (gapLength + dashLength);
		// A # of steps proportional to the gapLength are devoted to manipulating the gap
		var gapSteps = steps - dashSteps;

		// The current step #
		var step = 0;

		let start = undefined;
		function animate(timestamp) {
			if (start === undefined)
				start = timestamp;

			step = (timestamp - start) / 25;
			//step = step + 1;
			if (step >= steps) start = timestamp;

			var t, a, b, c, d;
			if (step < dashSteps) {
				t = step / dashSteps;
				a = (1 - t) * dashLength;
				b = gapLength;
				c = t * dashLength;
				d = 0;
			} else {
				t = (step - dashSteps) / (gapSteps);
				a = 0;
				b = (1 - t) * gapLength;
				c = dashLength;
				d = t * gapLength;
			}

			map.setPaintProperty("lines_top", "line-dasharray", [a, b, c, d]);

			window.requestAnimationFrame(animate);
		};

		window.requestAnimationFrame(animate);
	}


}); // end: load


function loadErrorImage(id) {
	if (map.hasImage(id)) return;
	const rgb = [255, 100, 100];

	const width = 42; // The image will be 64 pixels square.
	const bytesPerPixel = 4; // Each pixel is represented by 4 bytes: red, green, blue, and alpha.
	const data = new Uint8Array(width * width * bytesPerPixel);

	for (let x = 0; x < width; x++) {
		for (let y = 0; y < width; y++) {
			const offset = (y * width + x) * bytesPerPixel;
			data[offset + 0] = rgb[0]; // red
			data[offset + 1] = rgb[1]; // green
			data[offset + 2] = rgb[2]; // blue
			data[offset + 3] = 255; // alpha
		}
	}

	map.addImage(id, { width: width, height: width, data: data });
}

function toggle3D(newState = undefined) {
	let state = map.getLayoutProperty('Building 3D', 'visibility') == "visible";
	if (newState == undefined)
		newState = !state;
	document.getElementById("toggle-3d").textContent = newState ? "2D" : "3D";
	localStorage.setItem("3d_buildings", newState)
	if (state && !newState) {
		map.setLayoutProperty('Building 3D', 'visibility', 'none');
		map.setLayerZoomRange("building", 14, 23);
	} else if (!state && newState) {
		map.setLayoutProperty('Building 3D', 'visibility', 'visible');
		map.setLayerZoomRange("building", 12, 15);
	}
}

function setVector(use_vector) {
	if (USE_VECTOR == use_vector) return;
	localStorage.setItem("USE_VECTOR", use_vector);
	window.location.reload();
}