const $ = document.querySelector.bind(document)

const openSheetButton = $("#open-sheet")
const sheet = $("#sheet")
const sheetContents = sheet.querySelector(".contents")
const sheetBody = sheet.querySelector(".body")
// const draggableArea = sheet.querySelector(".draggable-area")

let sheetHeight = 0; // in vh

function setSheetHeight(value) {
  if (sheetHeight == value) return

  // Freshly opened
  if (sheetHeight == 0) {
    const myEvent = new CustomEvent("sheet-open");
    sheet.dispatchEvent(myEvent);

    sheet.setAttribute("aria-hidden", "false");
  } 
  // Closed
  else if (value == 0) {
    const myEvent = new CustomEvent("sheet-close");
    sheet.dispatchEvent(myEvent);

    sheet.setAttribute("aria-hidden", "true");
  }
  
  sheetHeight = Math.max(0, Math.min(100, value))
  sheetContents.style.height = `${sheetHeight}vh`

  sheetContents.classList.toggle("fullscreen", sheetHeight === 100)
}

// Hide the sheet when clicking the 'close' button
sheet.querySelector(".close-sheet").addEventListener("click", () => {
  setSheetHeight(0)
})

const isFocused = element => document.activeElement === element

// Hide the sheet when pressing Escape if the target element
// is not an input field
window.addEventListener("keyup", (event) => {
  const isSheetElementFocused =
    sheet.contains(event.target) && isFocused(event.target)

  if (event.key === "Escape" && !isSheetElementFocused) {
    setSheetHeight(0)
  }
})

const touchPosition = (event) =>
  event.touches ? event.touches[0] : event

let dragPosition

const onDragStart = (event) => {
  dragPosition = touchPosition(event).pageY
  // lastTouchTime = Date.now();
  sheetContents.classList.add("not-selectable")
  sheetContents.style.cursor = document.body.style.cursor = "grabbing"
}

// let lastTouchTime = 0;
// let velocity = 0;

const onDragMove = (event) => {
  const y = touchPosition(event).pageY
  if (dragPosition === undefined) return
  if (sheetContents.scrollTop != 0) return

  const deltaY = dragPosition - y
  const deltaHeight = deltaY / window.innerHeight * 100

  // let time = Date.now()
  // velocity = deltaHeight / (time - lastTouchTime)
  // lastTouchTime = time

  setSheetHeight(sheetHeight + deltaHeight)
  dragPosition = y
}

const onDragEnd = (event) => {
  console.debug(event)
  dragPosition = undefined
  sheetContents.classList.remove("not-selectable")
  sheetContents.style.cursor = document.body.style.cursor = ""

  // let velocitySheetHeight = sheetHeight + (velocity * 100)
  // velocity = 0
  // sheetHeight = velocitySheetHeight

  if (sheetHeight < 25) {
    setSheetHeight(0)
  } else if (sheetHeight > 75) {
    setSheetHeight(100)
  } else {
    setSheetHeight(sheetHeight)
  }
}

// draggableArea.addEventListener("mousedown", onDragStart)
// draggableArea.addEventListener("touchstart", onDragStart)

sheetContents.addEventListener("mousedown", onDragStart)
sheetContents.addEventListener("touchstart", onDragStart)

window.addEventListener("mousemove", onDragMove)
window.addEventListener("touchmove", onDragMove)

window.addEventListener("mouseup", onDragEnd)
window.addEventListener("touchend", onDragEnd)
window.addEventListener("touchcancel", onDragEnd)
