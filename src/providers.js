let API_BASE = "https://api.modra.ninja";
let OJPP_BASE = "https://ojpp.si";

if (window.location.href.includes("127.0.0.1")) {
	// API_BASE = "http://127.0.0.1:42066";
	// OJPP_BASE = "http://127.0.0.1:8000";
}

let BASE_ICON_SIZE = .6;

var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
const CACHE_STORAGE = localStorage;

class Provider {
	api_url = API_BASE;

	id = undefined;
	group = "unsorted";

	api_name = undefined;
	provider_name = undefined;

	hasHeading = false;
	hasCapacity = false;
	hasCustomIcon = false;
	refreshTime = 0;

	initialized = false;
	visible = false;

	iconSize = BASE_ICON_SIZE;
	allowOverlap = true;
	useCache = true;

	async fetch_geojson(coords) {
		let data = await fetch(this.api_url + this.endpoint);
		data = await data.json();

		let doCache = this.useCache && !isSafari;
		if (doCache) {
			let cache = JSON.stringify(data);
			if (this.hasCapacity)
				cache = cache.replaceAll(/"(capacity|capacity_free|vehicles_available)":[0-9]+/g, '"$1": ""');
			CACHE_STORAGE.setItem("cache__" + this.id, cache);
		}

		return data;
	}

	getImages() {
		let imgs = [this.id];
		if (this.hasCapacity)
			imgs.push(this.id + '_capacity')
		if (this.image_id)
			imgs.push(this.image_id);
		return imgs;
	}

	async fetch_geojson_cached() {
		let doCache = this.useCache && !isSafari;
		let str = CACHE_STORAGE.getItem("cache__" + this.id);
		// If a cache is available, return from it, otherwise fetch now
		if (doCache && str !== null && str != "null") {
			let geojson = JSON.parse(str);
			setTimeout(() => this.refresh(), 100);
			return geojson;
		} else {
			let geojson = await this.fetch_geojson();
			return geojson;
		}
	}

	async make_layer() {
		let layer = {
			id: this.id,
			type: "symbol",
			cluster: true,
			layout: {
				"icon-image": [
					"coalesce",
					["image", this.image_id ? this.image_id : this.id],
					["image", "error_image"],
				],
				"icon-size": [
					'interpolate',
					['linear'],
					['zoom'],
					12,
					this.iconSize * .70,
					13,
					this.iconSize,
				],
				"icon-allow-overlap": this.allowOverlap,
			},
		};

		if (this.hasCapacity) {
			let capacityShowZoom = 13.4;
			Object.assign(layer.layout, {
				"text-allow-overlap": true,
				"text-size": [
					"step",
					["zoom"],
					0,
					capacityShowZoom, // min zoom
					10.5, // text size
				],
				//"icon-text-fit": "width",
				"text-field": [
					"format",
					["get", "vehicles_available"],
					{ "text-color": "green" },
					" : ",
					{ "text-color": "gray" },
					["get", "capacity_free"],
					{ "text-color": "red" },
				],
				// "icon-size": [
				// 	"interpolate",
				// 	["linear"],
				// 	["zoom"],
				// 	13, this.iconSize * .8,
				// 	13.1, this.iconSize * 1.2,
				// 	20, this.iconSize * 2,
				// ],
				"icon-size": [
					"step",
					["zoom"],
					this.iconSize * .8,
					capacityShowZoom, // min zoom
					this.iconSize * 1.5,
				],
				"icon-image": [
					"step",
					["zoom"],
					[
						"coalesce",
						["image", this.id],
						["image", "error_image"],
					],
					capacityShowZoom, // min zoom
					[
						"coalesce",
						["image", this.id + '_capacity'],
						["image", "error_image"],
					],
				],
			});
			layer.paint = {
				"text-translate": [0, -10],
				"text-translate-anchor": "viewport",
				"text-halo-color": "white",
				"text-halo-width": 3,
			};
		}

		if (this.hasHeading) layer.layout["icon-rotate"] = ["get", "direction"];

		return layer;
	}

	async init() {
		this.initialized = true;
		let geojsonObject = { "type": "FeatureCollection", "features": [] };
		let layerName = this.id;

		let layer = await this.make_layer();

		layer.source = { type: "geojson", data: geojsonObject };

		map.addLayer(layer);

		map.on("click", layerName, async (e) => {
			let panel = await this.getPanel(e.features[0]);
			if (panel != null) {
				showInfoPanel(1, panel);
			}
		});

		map.on("mouseenter", layerName, async (e) => {
			map.getCanvas().style.cursor = "pointer";
			console.debug(e.features)
			let html = await this.getPopup(e.features[0]);

			var coordinates = e.features[0].geometry.coordinates.slice();
			if (html != null)
				popup.setLngLat(coordinates).setHTML(html).addTo(map);
		});

		// Change it back to a pointer when it leaves.
		map.on("mouseleave", layerName, function () {
			map.getCanvas().style.cursor = "";
			popup.remove();
		});
	}

	async refresh(first=false) {
		if (!this.visible) return;
		let geojson = first ? await this.fetch_geojson_cached() : await this.fetch_geojson();
		map.getSource(this.id).setData(geojson);
	}

	async getPopup(feature) {
		if (feature.properties.title)
			return feature.properties.title;
		if (feature.properties.name)
			return feature.properties.name;
		return feature.id + " (" + this.id + ")";
	}

	async getPanel(feature) {
		return null;
	}

	async setVisible(visible) {
		if (visible && !this.visible) {
			map.setLayoutProperty(this.id, "visibility", "visible");
			this.visible = true;
			// await this.refresh(true);
			this.refresh(true).then(console.debug);
		} else if (!visible && this.visible) {
			map.setLayoutProperty(this.id, "visibility", "none");
			this.visible = false;
		}
		return this.visible;
	}

	async toggle() {
		await this.setVisible(!this.visible);
		return this.visible;
	}
}

class LPP_StationProvider extends Provider {
	api_url = "https://lpp.ojpp.derp.si";
	line_colors = {};
	iconSize = BASE_ICON_SIZE * .9;
	allowOverlap = false;
	arrival_types = {"0": `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M429.6 92.1c4.9-11.9 2.1-25.6-7-34.7s-22.8-11.9-34.7-7l-352 144c-14.2 5.8-22.2 20.8-19.3 35.8s16.1 25.8 31.4 25.8H224V432c0 15.3 10.8 28.4 25.8 31.4s30-5.1 35.8-19.3l144-352z"/></svg>&nbsp;`,
		 "1": ``, 
		 "2": html`<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M.3 166.9L0 68C0 57.7 9.5 50.1 19.5 52.3l35.6 7.9c10.6 2.3 19.2 9.9 23 20L96 128l127.3 37.6L181.8 20.4C178.9 10.2 186.6 0 197.2 0h40.1c11.6 0 22.2 6.2 27.9 16.3l109 193.8 107.2 31.7c15.9 4.7 30.8 12.5 43.7 22.8l34.4 27.6c24 19.2 18.1 57.3-10.7 68.2c-41.2 15.6-86.2 18.1-128.8 7L121.7 289.8c-11.1-2.9-21.2-8.7-29.3-16.9L9.5 189.4c-5.9-6-9.3-14-9.3-22.5zM32 448H608c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32zm96-80a32 32 0 1 1 64 0 32 32 0 1 1 -64 0zm128-16a32 32 0 1 1 0 64 32 32 0 1 1 0-64z"/></svg>&nbsp;`, 
		 "3": "OBVOZ"}
	async fetch_geojson() {
		let r = await fetch(this.api_url + "/api/station/station-details");
		let json = await r.json();
		let geojson = geojson_convert(json.data, {
			id: "int_id",
			lat: "latitude",
			lng: "longitude",
		});
		return geojson;
	}
	async getPopup(feature) {
		let station = feature.properties;

		let routes = '';
		for (let line of JSON.parse(station.route_groups_on_station))
			routes += html`<span class="line line-${this.id} line-${line}" style="background-color:#${this.line_colors[line.replaceAll(/[^\d-]/g, "")] || 'aaa'}">${line}</span>`;

		return html`
			<strong>${station.name}</strong><br>
			${routes}
		`;
	}
	async getPanel(feature) {
		let station = feature.properties;

		let routes = '';
		for (let line of JSON.parse(station.route_groups_on_station))
			routes += html`<span class="line line-${this.id} line-${line}" style="background-color:#${this.line_colors[line.replaceAll(/[^\d-]/g, "")] || 'aaa'}">${line}</span>`;

		return {
			title: station.name,
			subtitle: `<div class="lines">${routes}</div>`
		};
	}
}

class LPP_LocationProvider extends Provider {
	api_url = "https://lpp.ojpp.derp.si";
	arrival_types = {
		"0": "",
		"1": "",
		"2": "Prihod",
		"3": "Na obvozu"
	}
	hasHeading = true;
	refreshTime = 15;
	useCache = false;
	async getRouteData() {
		if (this.route_data !== undefined) return this.route_data;
		let r = await fetch(
			this.api_url + "/api/route/active-routes"
		);
		let json = await r.json();
		this.route_data = json.data;
		return json.data;
	}
	async fetch_geojson() {
		let r = await fetch(this.api_url + "/api/bus/bus-details?trip-info=1");
		let json = await r.json();
		let route_data = await this.getRouteData();
		json.data.map(x => {
			let route = route_data.find(y => y.trip_id == x.trip_id);
			if (route) {
				x.line_id = route.route_id;
				x.line_number = route.route_number;
				x.line_name = route.route_name;
				x.line_destination = route.short_route_name;
				x.direction = x.cardinal_direction
			}
		})
		//let data = json.data.filter(x => x.line_id !== undefined);
		let geojson = geojson_convert(json.data, {
			id: "bus_unit_id",
			lat: "coordinate_y",
			lng: "coordinate_x",
		});
		console.debug(geojson)
		return geojson;
	}
	async getPopup(feature) {
		let bus = feature.properties;
		return html`
			<strong><span class="line" style="background-color:#${this.line_colors[bus.line_number.replaceAll(/[^\d-]/g, "")] || 'aaa'}">${bus.line_number}</span> ${bus.line_destination.length > 0 ? bus.line_destination : bus.line_name.toUpperCase()}</strong><br>
		`;
	}
	async getPanel(feature) {
		let bus = feature.properties;
		if (bus.trip_id)
			this.showTrip(bus);
		let trip_details = await this.getTripDetails(bus.trip_id, bus.bus_unit_id.toLowerCase());
		let arr_str = "";
		for (let arrival of trip_details.arrivals) {
			if (arrival.is_first || arrival.is_last) {
				arrival.stop = arrival.stop.toUpperCase()
			}
			if (arrival.is_first) {
				arr_str += html`
				<div class="bus-arrival-stop">
					<div class="bus-arrival-stop-icon">
						<img src="https://mestnipromet.cyou/tracker/img/ico/verticalStartStation.png" />
					</div>
					<div class="bus-arrival-stop-name">
						<span style="font-weight:bold">${arrival.stop}</span>
					</div>
					<div class="bus-arrival-stop-eta">
						<span>${arrival.type < 2 ? `${arrival.eta_min} min` : this.arrival_types[arrival.type]}</span>
					</div>
				</div>`
			} else if (arrival.is_last) {
				arr_str += html`
				<div class="bus-arrival-stop">
					<div class="bus-arrival-stop-icon">
						<img src="https://mestnipromet.cyou/tracker/img/ico/verticalFinalStation.png" />
					</div>
					<div class="bus-arrival-stop-name">
						<span style="font-weight:bold">${arrival.stop}</span>
					</div>
					<div class="bus-arrival-stop-eta">
						<span>${arrival.type < 2 ? `${arrival.eta_min} min` : this.arrival_types[arrival.type]}</span>
					</div>
				</div>`
			} else {
				arr_str += html`
				<div class="bus-arrival-stop">
					<div class="bus-arrival-stop-icon">
						<img src="https://mestnipromet.cyou/tracker/img/ico/verticalGreenStation.png" />
					</div>
					<div class="bus-arrival-stop-name">
						<span>${arrival.stop}</span>
					</div>
					<div class="bus-arrival-stop-eta">
						<span>${arrival.type < 2 ? `${arrival.eta_min} min` : this.arrival_types[arrival.type]}</span>
					</div>
				</div>`
			}
		}
		return {
			title: html`<span class="line" style="background-color:#${this.line_colors[bus.line_number.replaceAll(/[^\d-]/g, "")] || 'aaa'}">${bus.line_number}</span> ${bus.line_destination.length > 0 ? bus.line_destination : bus.line_name.toUpperCase()} ${trip_details.to_depot ? "(za garažo)" : ""}`,
			image: { src: `https://mestnipromet.cyou/tracker/img/avtobusi/${bus.name.split("-")[1]}.jpg` },
			body: html`
				<h4>Podatki o avtobusu:</h4>
				<div class="bus-info">
					<div class="bus-info-item">
						<div class="bus-info-item-label">Registrska številka: </div>
						<div class="bus-info-item-value">${bus.name}</div>
					</div>
				</div>
				<hr>
				<h4>Prihodi na postaje:</h4>
				<div class="bus-arrival">
					${arr_str}
				</div>
			`
		};
	}
	async getTripDetails(trip_id, bus_id) {
		let data = await fetch(this.api_url + '/api/route/arrivals-on-route?trip-id=' + trip_id).then(r => r.json()).then(r => r.data);
		let arrivals = [];
		let to_depot = false;
		let was_skipped = false;
		for (let stop of data) {
			let mentioned = false;
			for (let arrival of stop.arrivals) {
				if (arrival.vehicle_id == bus_id) {
					mentioned = true;
					if (was_skipped) {
						arrivals = [];
					}
					arrivals.push({
						stop: stop.name,
						code: stop.station_code,
						sequence: stop.order_no,
						is_last: data[data.length - 1].station_code == stop.station_code,
						is_first: data[0].station_code == stop.station_code,
						eta_min: arrival.eta_min,
						type: arrival.type,
					});
					if (arrival.depot == 1) to_depot = true;
					break;
				}
			}
			if (!mentioned && !was_skipped) {
				if (arrivals.length > 0) {
					was_skipped = true;
				}
			}
		}
		return {
			arrivals,
			to_depot
		};
	}
	async make_layer() {
		let layer = await super.make_layer();
		layer.filter = ["!=", ["get", "trip_id"], null];
		Object.assign(layer.layout, {
			"text-allow-overlap": false,
			"icon-size": this.iconSize,
			"text-size": [
				"step",
				["zoom"],
				0,
				12, // min zoom
				9.5, // text size
			],
			"text-field": ["get", "line_number"],
			"text-font": ["Open Sans Bold", "Arial Unicode MS Bold"],
			"icon-rotation-alignment": "map",
			"icon-size": [
				'interpolate',
				['linear'],
				['zoom'],
				8, this.iconSize * .5,
				10, this.iconSize,
				20, this.iconSize * 1.5,
			],
		});

		layer.paint = {
			"text-translate": [0, 0],
			"text-translate-anchor": "viewport",
			"text-halo-color": "#1d7b4e",
			"text-halo-width": 3,
			"text-color": "white",

		};

		return layer;
	}
	async showTrip(bus) {
		let r = await fetch('https://lpp.ojpp.derp.si/api/route/routes?shape=1&route_id=' + bus.line_id);
		let json = await r.json();
		let correctRoute = json.data.filter(x => x.trip_id == bus.trip_id);
		if (correctRoute.length == 0) return map.getSource('lines').setData({ type: "FeatureCollection", features: [] });
		let shape = correctRoute[0].geojson_shape;
		let geojson = {
			'type': 'Feature',
			'properties': {
				'color': '#' + (this.line_colors[bus.line_number.replaceAll(/[^\d-]/g, "")] || 'aaa'),
			},
			'geometry': shape
		};
		map.getSource('lines').setData(geojson);
		//zoomToGeoJSON({features:[geojson]});
	}
}

let lpp_line_colors = { "1": "C93336", "2": "8C8841", "3": "EC593A", "5": "9F539E", "6": "939598", "7": "1CBADC", "8": "116AB0", "9": "86AACD", "11": "EDC23B", "12": "214AA0", "13": "CFD34D", "14": "EF59A1", "15": "A2238E", "18": "895735", "19": "EA9EB4", "20": "1F8751", "21": "52BA50", "22": "F6A73A", "24": "ED028C", "25": "0F95CA", "26": "231F20", "27": "57A897", "30": "9AD2AE", "40": "496E6D", "42": "A78B6B", "43": "4E497A", "44": "817EA8", "51": "6C8BC6", "52": "00565D", "53": "C7B3CA", "56": "953312", "60": "ACBB71", "61": "F9A64A", "71": "6C8BC6", "72": "4CA391", "73": "FECA0A", "78": "C96D6A", "16": "582C81", "23": "40AE49", }
let marprom_line_colors = { '1': '000000', '2': '0081a7', '3': '00afb9', '4': 'f6bd60', '5': 'f07167', '6': '4a4e69', '7': 'c9ada7', '8': 'ddb892', '9': '7f5539', '10': '656d4a', '11': '000000', '12': '081c15', '13': 'e71d36', '14': '000000', '15': 'fe7f2d', '16': 'fcca46', '17': '720026', '18': '736f72', '19': 'b32d00', '20': 'e27396', '21': 'a3a375', '22': '5c85d6', '23': '68a691', '24': 'a6b1e1', '25': '02040f', }

class RideshareProvider extends Provider {
	text_vehicles = 'Kolesa';
	vehiclesEndpoint = undefined;

	async getPopup(feature) {
		let station = feature.properties;
		return html`
			<strong>${station.title}</strong><br>
			<strong>${this.text_vehicles}:</strong> ${station.vehicles_available} <strong>Prosto:</strong> ${station.capacity_free}
		`;
	}
	async getPanel(feature) {
		let station = feature.properties;
		let panel = {
			title: station.title,
			subtitle: station.address,
		}
		if (station.image_url)
			panel.image_url = station.image_url;

		panel.body = `
			<div>
				<strong>${this.text_vehicles}:</strong> ${station.vehicles_available} <strong>Prosto:</strong> ${station.capacity_free}
			</div>
			<hr>
			<div id="info-panel-vehicles-${station.id}" class="vehicle-list">${$_SPINNER}</div>
		`

		showInfoPanel(1, panel);

		if (this.vehiclesEndpoint)
			fetch(this.api_url + `${this.vehiclesEndpoint}?station_id=${station.id}`).then(r => r.json()).then(vehicles => {
				let $_vehicles = "";
				for (let vehicle of vehicles) {					
					$_vehicles += this.vehicle_row(vehicle);
				}
				document.getElementById("info-panel-vehicles-" + station.id).innerHTML = $_vehicles;
			})
	}

	getVehicleName(vehicle) {
		return vehicle.model
	}

	vehicle_row(vehicle) {
		let image = '';
		if (vehicle.image_url)
			image = `<img src="${imgProxy(vehicle.image_url, 128)}" alt="" class="vehicle-image">`;

		let deeplink_ico = '';
		if (vehicle.deeplink)
			deeplink_ico = html`<div class="deeplink-ico"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-chevron-right" style="--darkreader-inline-stroke: currentColor;" data-darkreader-inline-stroke=""><polyline points="9 18 15 12 9 6"></polyline></svg></div>`;

		let $_vehicle = html`
		<a class="vehicle"${vehicle.deeplink ? ' href="' + vehicle.deeplink + '"' : ''} target="_blank">
			${image}
			<div class="flex-main">
				<div><strong>${this.getVehicleName(vehicle)}</strong></div>
				<small>
				${vehicle.price_per_min ? vehicle.price_per_min + "&nbsp;€/min" : ""} 
				${vehicle.price_per_km ? vehicle.price_per_km + "&nbsp;€/km" : ""}	
				${vehicle.price_minimum ? "<br><strong>Mininum:</strong> " + vehicle.price_minimum + "&nbsp;€" : ""}
				${vehicle.price_start ? "<br><strong>Štartnina:</strong> " + vehicle.price_start + "&nbsp;€" : ""}
				</small>
			</div>
			<div class="flex-additional-data">
		`;
		if (vehicle.charge_level)
			$_vehicle += html`
				<div>
					<svg xmlns="http://www.w3.org/2000/svg" width="1em" viewBox="0 0 512 512" fill="currentColor"><path d="M80 96c0-17.7 14.3-32 32-32h64c17.7 0 32 14.3 32 32l96 0c0-17.7 14.3-32 32-32h64c17.7 0 32 14.3 32 32h16c35.3 0 64 28.7 64 64V384c0 35.3-28.7 64-64 64H64c-35.3 0-64-28.7-64-64V160c0-35.3 28.7-64 64-64l16 0zm304 96c0-8.8-7.2-16-16-16s-16 7.2-16 16v32H320c-8.8 0-16 7.2-16 16s7.2 16 16 16h32v32c0 8.8 7.2 16 16 16s16-7.2 16-16V256h32c8.8 0 16-7.2 16-16s-7.2-16-16-16H384V192zM80 240c0 8.8 7.2 16 16 16h96c8.8 0 16-7.2 16-16s-7.2-16-16-16H96c-8.8 0-16 7.2-16 16z"/></svg>
					&nbsp;
					<span>${vehicle.charge_level} %</span>
				</div>`;
		if (vehicle.range_estimate)
			$_vehicle += html`
				<div>
					<svg xmlns="http://www.w3.org/2000/svg" width="1em" viewBox="0 0 576 512" fill="currentColor"><path d="M256 32H181.2c-27.1 0-51.3 17.1-60.3 42.6L3.1 407.2C1.1 413 0 419.2 0 425.4C0 455.5 24.5 480 54.6 480H256V416c0-17.7 14.3-32 32-32s32 14.3 32 32v64H521.4c30.2 0 54.6-24.5 54.6-54.6c0-6.2-1.1-12.4-3.1-18.2L455.1 74.6C446 49.1 421.9 32 394.8 32H320V96c0 17.7-14.3 32-32 32s-32-14.3-32-32V32zm64 192v64c0 17.7-14.3 32-32 32s-32-14.3-32-32V224c0-17.7 14.3-32 32-32s32 14.3 32 32z"/></svg>
					&nbsp;
					<span>~${vehicle.range_estimate} km</span>
				</div>`;
		$_vehicle += html`
			</div>
			${deeplink_ico}
		</a>
		`;
		return $_vehicle;
	}
}

class JCDecauxBikeRideshareProvider extends RideshareProvider {
	getVehicleName(vehicle) {
		return vehicle.title;
	}

	vehicle_row(vehicle) {
		let image = '';
		if (vehicle.image_url)
			image = `<img src="${imgProxy(vehicle.image_url, 128)}" alt="" class="vehicle-image">`;

		let deeplink_ico = '';
		if (vehicle.deeplink)
			deeplink_ico = html`<div class="deeplink-ico"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-chevron-right" style="--darkreader-inline-stroke: currentColor;" data-darkreader-inline-stroke=""><polyline points="9 18 15 12 9 6"></polyline></svg></div>`;

		let $_vehicle = html`
		<a class="vehicle"${vehicle.deeplink ? ' href="' + vehicle.deeplink + '"' : ''} target="_blank">
			${image}
			<div class="flex-main">
				<div><small>${vehicle.number_in_station}.</small> <strong>${this.getVehicleName(vehicle)}</strong></div>
				<small>
					${(vehicle.rating / 10).toFixed(0)}/10 ⭐️
				</small>
			</div>
			<div class="flex-additional-data">
		`;

		$_vehicle += html`
			</div>
			${deeplink_ico}
		</a>
		`;
		return $_vehicle;
	}
}

class FloatingRideshareProvider extends Provider {
	async getPopup(feature) {
		let vehicle = feature.properties;
		return html`
			<strong>${vehicle.title}</strong><br>
			${vehicle.model}
		`;
	}
	async getPanel(feature) {
		let vehicle = feature.properties;
		let panel = {
			title: vehicle.title,
			subtitle: this.name,
		}
		if (vehicle.image_url)
			panel.image_url = vehicle.image_url;


		let $_vehicles = RideshareProvider.prototype.vehicle_row(vehicle);

		panel.body = `
			<div id="info-panel-vehicles-${feature.id}" class="vehicle-list"></div>
		` + $_vehicles;

		showInfoPanel(1, panel);

	}

}

const PROVIDERS = {
	lpp_postaje: new (class lpp_postaje extends LPP_StationProvider {
		id = "lpp_postaje";
		group = "mestni_postaje";
		name = "LPP postaje";
		line_colors = lpp_line_colors;
		async getPanel(feature) {
			//let panel = await super.getPanel(feature);
			let station = feature.properties;
			let arrivals = await this.getArrivals(station);
			let arrivals_str = "";
			for (let line of arrivals) {
				let route_num = line[1];
				let arrival = line[2] ;
				for (let route in arrival) {
					let route_name = route;
					let route_color = this.line_colors[line[0].toString()];
					route = arrival[route];
					arrivals_str += html`
						<div class="arrivals-route">
						<div class="arrivals-route-first-row">
							<div class="route-number" style="background-color: ${route_color && route_color != "" ? '#' + route_color : "var(--c-fg-primary);"}">
								${route_num}
							</div>
							<div class="route-name">
								${route_name.toUpperCase()}
							</div>
							<div class="route-first-arrival">
								${route[0].type < 2 ? `${this.arrival_types[route[0].type]} <div>${route[0].eta_min}&nbsp;min</div>` : this.arrival_types[route[0].type]}
							</div>
						</div>
						${route.length > 1 ? html`<div class="arrivals-route-second-row">
								${route[1] ? html`<div class="route-second-arrival">
								${route[1].type < 2 ? `${this.arrival_types[route[1].type]} <div>${route[1].eta_min}&nbsp;min</div>` : this.arrival_types[route[1].type]}
								</div>` : html``}
								${route[2] ? html`<div class="route-second-arrival">
								${route[2].type < 2 ? `${this.arrival_types[route[2].type]} <div>${route[2].eta_min}&nbsp;min</div>` : this.arrival_types[route[2].type]}
								</div>` : html``}
								${route[3] ? html`<div class="route-second-arrival">
								${route[3].type < 2 ? `${this.arrival_types[route[3].type]} <div>${route[3].eta_min}&nbsp;min</div>` : this.arrival_types[route[3].type]}
								</div>` : html``}
								${route[4] ? html`<div class="route-second-arrival">
								${route[4].type < 2 ? `${this.arrival_types[route[4].type]} <div>${route[4].eta_min}&nbsp;min</div>` : this.arrival_types[route[4].type]}
								</div>` : html``}
							</div>` : 
						html``}
					</div>
						`;
				}
			}
			// write to panel
			return {
				title: html`${station.name} <span class="center-badge">${parseInt(station.ref_id) % 2 == 0 ? 'Iz centra' : 'Proti centru'}</span>`,
				body: html`<h3>Prihodi</h3> 
					${arrivals_str}
				`
			};
		}
		async getArrivals(station) {
			let arrivals = await fetch(`${this.api_url}/api/station/arrival?station-code=${station.ref_id}`).then(r => r.json());
			arrivals = arrivals.data.arrivals;
			let tmp = {};
			for (let arrival of arrivals) {
				if (!tmp[arrival.route_name])
					tmp[arrival.route_name] = {};
				if (!tmp[arrival.route_name][arrival.stations.arrival])
					tmp[arrival.route_name][arrival.stations.arrival] = [];
				tmp[arrival.route_name][arrival.stations.arrival].push(arrival);
			}
			// sort by keys firstly as int then as string, return as array
			let tmp_arr = []
			for (let route in tmp) {
				let route_num = route.replaceAll(/[^\d-]/g, "");
				tmp_arr.push([parseInt(route_num), route, tmp[route]]);
			}
			tmp_arr.sort((a, b) => {
				if (a[0] == b[0])
					return a[1] > b[1] ? 1 : -1;
				return a[0] - b[0];
			});
			return tmp_arr;
		}
	})(),
	marprom_postaje: new (class marprom_postaje extends LPP_StationProvider {
		id = "marprom_postaje";
		group = "mestni_postaje";
		name = "Marprom postaje";
		api_url = "https://marprom-lpp.ojpp.derp.si";
		line_colors = marprom_line_colors;

		async getPanel(feature) {
			let panel = await super.getPanel(feature);
			let station = feature.properties;
			let arrivals = await this.getArrivals(station);
			let arrivals_str = "";
			for (let line of arrivals) {
				let route_num = line[1];
				let arrival = line[2] ;
				for (let route in arrival) {
					let route_name = route;
					let route_color = this.line_colors[line[0].toString()];
					console.log(this.line_colors, route_color)
					route = arrival[route];
					arrivals_str += html`
						<div class="arrivals-route">
						<div class="arrivals-route-first-row">
							<div class="route-number" style="background-color: ${route_color && route_color != "" ? '#' + route_color : "var(--c-fg-primary);"}">
								${route_num}
							</div>
							<div class="route-name">
								${route_name.toUpperCase()}
							</div>
							<div class="route-first-arrival">
								${route[0].type < 2 ? `${this.arrival_types[route[0].type]} <div>${route[0].eta_min}&nbsp;min</div>` : this.arrival_types[route[0].type]}
							</div>
						</div>
						${route.length > 1 ? html`<div class="arrivals-route-second-row">
								${route[1] ? html`<div class="route-second-arrival">
								${route[1].type < 2 ? `${this.arrival_types[route[1].type]} <div>${route[1].eta_min}&nbsp;min</div>` : this.arrival_types[route[1].type]}
								</div>` : html``}
								${route[2] ? html`<div class="route-second-arrival">
								${route[2].type < 2 ? `${this.arrival_types[route[2].type]} <div>${route[2].eta_min}&nbsp;min</div>` : this.arrival_types[route[2].type]}
								</div>` : html``}
								${route[3] ? html`<div class="route-second-arrival">
								${route[3].type < 2 ? `${this.arrival_types[route[3].type]} <div>${route[3].eta_min}&nbsp;min</div>` : this.arrival_types[route[3].type]}
								</div>` : html``}
								${route[4] ? html`<div class="route-second-arrival">
								${route[4].type < 2 ? `${this.arrival_types[route[4].type]} <div>${route[4].eta_min}&nbsp;min</div>` : this.arrival_types[route[4].type]}
								</div>` : html``}
							</div>` : 
						html``}
					</div>
						`;
				}
			}
			// if (station.station_num && station.station_num != "null")
			// 	panel.image = { src: `https://www.marprom.si/webmap/website/assets/img/postaje/s${station.station_num}.jpg` };

			panel.body = html`<h3>Prihodi</h3>
				${arrivals_str}
			`;
			return panel;
		}
		async getArrivals(station) {
			let arrivals = await fetch(`https://marprom-lpp.ojpp.derp.si/api/station/arrival?station-code=${station.ref_id}`).then(r => r.json());
			arrivals = arrivals.data.arrivals;
			let tmp = {};
			for (let arrival of arrivals) {
				if (!tmp[arrival.route_name])
					tmp[arrival.route_name] = {};
				if (!tmp[arrival.route_name][arrival.stations.arrival])
					tmp[arrival.route_name][arrival.stations.arrival] = [];
				tmp[arrival.route_name][arrival.stations.arrival].push(arrival);
			}
			// sort by keys firstly as int then as string, return as array
			let tmp_arr = []
			for (let route in tmp) {
				let route_num = route.replaceAll(/[^\d-]/g, "");
				tmp_arr.push([parseInt(route_num), route, tmp[route]]);
			}
			tmp_arr.sort((a, b) => {
				if (a[0] == b[0])
					return a[1] > b[1] ? 1 : -1;
				return a[0] - b[0];
			});
			return tmp_arr;
		}
	})(),
	arriva_koper_postaje: new (class arriva_koper_postaje extends Provider {
		group = "mestni_postaje";
		id = "arriva_koper_postaje";
		name = "Arriva Koper postaje";
		image_id = "arriva_postaje";
		iconSize = BASE_ICON_SIZE * .9;
		allowOverlap = false;
		async fetch_geojson() {
			let res = await fetch('https://cors.proxy.prometko.si/http://pic.tekaso.si/geoserver/koper/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=koper%3AAvtobusnaPostajalisca&outputFormat=application%2Fjson');
			let geojson = await res.json();
			return geojson;
		}
		async getPopup(feature) {
			let station = feature.properties;
			return html`
				<strong>${station.ime_postaje}</strong>
			`;
		}
	})(),
	nomago_celje_postaje: new (class nomago_celje_postaje extends Provider {
		group = "mestni_postaje";
		id = "nomago_celje_postaje";
		name = "Nomago Celje postaje";
		image_id = "nomago_postaje";
		iconSize = BASE_ICON_SIZE * .9;
		allowOverlap = false;
		async fetch_geojson() {
			let res = await fetch('https://cors.proxy.prometko.si/https://prostor.celje.si/ows/public/wfs?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&TYPENAMES=public:int_promet_bus_postaje&outputFormat=application%2Fjson&srsName=EPSG:4326');
			let geojson = await res.json();
			return geojson;
		}
		async getPopup(feature) {
			let station = feature.properties;
			return html`
				<strong>${station.ime_postaje}</strong>
			`;
		}
	})(),
	nomago_bikes: new (class nomago_bikes extends RideshareProvider {
		id = "nomago_bikes";
		group = "nomago_bikes";
		endpoint = "/nextbike/SI/stations";
		vehiclesEndpoint = "/nextbike/vehicles";
		name = "Nomago Bikes";
		hasCapacity = true;
	})(),
	sharengo: new (class sharengo extends FloatingRideshareProvider {
		id = "sharengo";
		group = "sharengo";
		endpoint = "/sharengo/cars";
		name = "Share'N Go";
		useCache = false;
	})(),
	greengo: new (class greengo extends FloatingRideshareProvider {
		id = "greengo";
		group = "greengo";
		endpoint = "/greengo/vehicles";
		name = "GreenGo";
		useCache = false;
	})(),
	bicikelj: new (class bicikelj extends JCDecauxBikeRideshareProvider {
		id = "bicikelj";
		group = "europlakat";
		endpoint = "/jcdecaux/ljubljana/stations";
		vehiclesEndpoint = "/jcdecaux/ljubljana/vehicles";
		name = "BicikeLJ";
		hasCapacity = true;
	})(),
	mbajk: new (class mbajk extends JCDecauxBikeRideshareProvider {
		id = "mbajk";
		group = "europlakat";
		endpoint = "/jcdecaux/maribor/stations";
		vehiclesEndpoint = "/jcdecaux/maribor/vehicles";
		name = "MBajk";
		hasCapacity = true;
	})(),
	avant2go: new (class avant2go extends RideshareProvider {
		id = "avant2go";
		group = "avant2go";
		endpoint = "/avant2go/stations";
		vehiclesEndpoint = "/avant2go/vehicles";
		name = "Avant2Go";
		hasCapacity = true;
		text_vehicles = 'Vozila'
	})(),
	lpp_lokacije: new (class lpp_lokacije extends LPP_LocationProvider {
		id = "lpp_lokacije";
		group = "mestni_lokacije";
		name = "LPP lokacije";
		line_colors = lpp_line_colors;
	})(),
	marprom_lokacije: new (class marprom_lokacije extends LPP_LocationProvider {
		id = "marprom_lokacije";
		group = "mestni_lokacije";
		name = "Marprom lokacije";
		api_url = "https://marprom-lpp.ojpp.derp.si";
		line_colors = marprom_line_colors;
	})(),
	sz_lokacije: new (class sz_lokacije extends Provider {
		id = "sz_lokacije";
		group = "sz_lokacije";
		name = "SŽ lokacije";
		refreshTime = 16;
		useCache = false;
		async fetch_geojson() {
			let r = await fetch(
				"https://api.map.vlak.si/SI/sz/trips/active"
			);
			let json = await r.json();
			let features = [];
			for (let item of json.data) {
				features.push({
					"type": "Feature",
					"geometry": { "coordinates": [item['coordinates']['lng'], item['coordinates']['lat']], "type": "Point" },
					"properties": {
						...item.train_data,
						...item.train_cache
					},
					"id": item['train_number'],
					"bbox": null
				})
			}
			return { "type": "FeatureCollection", "features": features };
		}
		async getPopup(feature) {
			let train = feature.properties;
			return html`
				<strong>${train.train_name}</strong><br>
				<strong>Vlak:</strong> ${train.train_type} ${train.train_number}<br>
				${train.delay ? `<strong>Zamuda:</strong> <span style="color: red; font-weight: bold;">${train.delay}&nbsp;min</span>` : ""}
			`;
		}
		async getPanel(feature) {
			let train = feature.properties;
			let title = train.train_type + " " + train.train_no;
			setTimeout(() => this.showTrip(train), 0);
			let panel = {
				subtitle: "<small>Podatki o lokaciji so le približni!</small>",
			}
			if (train.train_model) {
				panel.title = `<img src="https://mestnipromet.cyou/tracker/img/sz/mini/sz${train.train_model.slice(0, 3)}.png" onerror="this.onerror=null;this.src='https://mestnipromet.cyou/tracker/img/sz/mini/unknown.png'">${title} <br><small>${train.route}</small>`;
			}
			return panel;
		}
		async showTrip(train) {
			let r = await fetch('https://mestnipromet.cyou/api/v1/resources/sz/geometry?route=' + train.train_no);
			let json = await r.json();
			let coords = fixCoordOrder(json.data);
			let geojson = {
				'type': 'Feature',
				'properties': {
					'color': '#3da5e7',
				},
				'geometry': {
					"type": "LineString",
					"coordinates": coords,
				}
			};
			map.getSource('lines').setData(geojson);
			//zoomToGeoJSON({features:[geojson]});
		}
		async make_layer() {
			let layer = await super.make_layer();
			let capacityShowZoom = 12;
			Object.assign(layer.layout, {
				"text-allow-overlap": false,
				"icon-size": this.iconSize,
				"text-size": [
					"step",
					["zoom"],
					0,
					capacityShowZoom, // min zoom
					9.5, // text size
				],
				//"text-field": ["get", "train_number"],
				"text-field": [
					"concat",
					["get", "train_type"],
					"",
					["get", "train_number"],
				],
				"text-font": ["Open Sans Bold", "Arial Unicode MS Bold"],
				"icon-rotation-alignment": "map",
				"icon-size": this.iconSize,
			});

			layer.paint = {
				"text-translate": [0, 12],
				"text-translate-anchor": "viewport",
				"text-halo-color": "#1aacef",
				"text-halo-width": 3,
				"text-color": "white",
			};

			return layer;
		}
	})(),
	ijpp_lokacije: new (class ijpp_lokacije extends Provider {
		id = "ijpp_lokacije";
		group = "ijpp_lokacije";
		name = "IJPP lokacije";
		refreshTime = 10;
		useCache = false;
		iconSize = BASE_ICON_SIZE * 1.2;
		hasHeading = true;
		async fetch_geojson() {
			let r = await fetch(
				`${OJPP_BASE}/api/vehicle_locations?active=1&exclude_operators=lpp,marprom`
			);
			return await r.json()
		}
		async getPopup(feature) {
			let vehicle = feature.properties;
			return html`
				<strong>${vehicle.route_name ? vehicle.route_name : "Neznana linija"}</strong><br>
			`;
		}
		async getPanel(feature) {
			let vehicle = feature.properties;
			setTimeout(() => this.showTrip(vehicle), 0);
			setTimeout(() => this.showTripArrivals(vehicle), 200);
			let vehicle_details = await this.getVehicleDetails(vehicle);
			console.log(vehicle_details)
			let panel = {
				title: `<img src="${OJPP_BASE}/api/operators/${vehicle.operator_id}/logo.png" style="height:32px;">${vehicle.route_name ? vehicle.route_name : "Neznana linija"}`,
				subtitle: vehicle.operator_name,
				image: {src: vehicle_details.photos ? `https://ojpp.si${vehicle_details.photos[0]}` : `https://mestnipromet.cyou/tracker/img/avtobusi/nima.jpg`},
				body: `<div>
				<strong>oJPP:</strong> <a href="${OJPP_BASE}/vehicles/${vehicle.vehicle_id}" target="_blank">#${vehicle.vehicle_id}</a><br>
				${vehicle_details.operator_vehicle_id ? `<strong>Interna številka:</strong> ${vehicle_details.operator_vehicle_id}<br>` : ""}
				${vehicle_details.plate ? `<strong>Registrska številka:</strong> ${vehicle_details.plate}<br>` : ""}
				${vehicle_details.model ? `<strong>Model:</strong> ${vehicle_details.model}<br>` : ""}
				<hr>
				<strong>Linija:</strong>${vehicle.route_name ? vehicle.route_name : "Neznana linija"}<br>
				</div>
				<div class="info-panel-wide" id="arrivals-${vehicle.vehicle_id}">${$_SPINNER}</div>
				`,
			};
			return panel;
		}
		async getVehicleDetails(vehicle) {
			let data = await fetch(`${OJPP_BASE}/api/vehicles/${vehicle.vehicle_id}/details/`).then(r => r.json());
			return data;
		}
		async showTripArrivals(vehicle) {
			let r = await fetch(`${OJPP_BASE}/api/trips/${vehicle.trip_id}/details/`);
			let trip = await r.json();
			let $_details = '';

			// iterate over stop_times
			for (let time of trip.stop_times) {
				let iso_date = time.time_departure || time.time_arrival;
				let passed = false;
				if (iso_date) {
					let date = new Date(new Date().toISOString().split('T')[0] + 'T' + iso_date);
					passed = date < new Date();
				}
				$_details += `
				<div class="detail-row bean ${passed ? 'passed' : ''}" id="stop-${time.stop.id}" onclick="createStopSidebar({id: ${time.stop.id}})">
					<div class="detail-stop-name">${time.stop.name}</div>
					<div class="detail-stop-time">${time.time_arrival != null ? time.time_arrival : '-'}<br>${time.time_departure != null && time.time_departure != '00:00' ? time.time_departure : '-'}</div>
				</div>
				`
			}

			document.getElementById("arrivals-" + vehicle.vehicle_id).innerHTML = `
				<table class="sidebar-table" >
					<tr>
						<th>${VOCABULARY.trip_details}</th>
					</tr>
				</table>
				${$_details}
			`
		}
		async showTrip(vehicle) {
			let r = await fetch(`${OJPP_BASE}/api/trips/${vehicle.trip_id}/geometry/`);
			let json = await r.json();
			let geojson = {
				'type': 'Feature',
				'properties': {
					'color': '#3da5e7',
				},
				'geometry': json
			};
			map.getSource('lines').setData(geojson);
			//zoomToGeoJSON({features:[geojson]});
		}
	})(),
	ojpp_postaje: new (class ojpp_postaje extends Provider {
		id = "ojpp_postaje";
		group = "ojpp_postaje";
		name = "oJPP postaje";
		useCache = false;
		hasHeading = true;
		iconSize = BASE_ICON_SIZE * .8;
		async fetch_geojson() {
			let r = await fetch(`${OJPP_BASE}/api/stop_locations`);
			return await r.json()
		}
		async getPopup(feature) {
			let stop = feature.properties;
			return html`
				<strong>${stop.name}</strong><br>
			`;
		}
		async getPanel(feature) {
			let stop = feature.properties;
			setTimeout(() => this.showArrivals(stop), 0);
			let panel = {
				title: stop.name,
				body: `
				<div>
					<strong>oJPP:</strong> <a href="${OJPP_BASE}/stop_locations/${stop.id}" target="_blank">#${stop.id}</a><br>
				</div>
				<div class="info-panel-wide" id="stop-arrivals-${stop.id}">${$_SPINNER}</div>
				`,
			};
			if (this.photo)
				panel.image = { src: OJPP_BASE + this.photo };
			return panel;
		}
		async showArrivals(stop) {
			let $_details = ""
			try {
				let arrivals = await fetch(`${OJPP_BASE}/api/stop_locations/${stop.id}/arrivals`).then(r => r.json());

				$_details = "<tr><td>";
				for (let time of arrivals) {
					let iso_date = time.time_departure || time.time_arrival;
					let passed = false;
					if (iso_date) {
						let date = new Date(new Date().toISOString().split('T')[0] + 'T' + iso_date);
						passed = date < new Date();
					}
					$_details += `
                <div class="detail-row ${passed ? 'passed' : ''}" id="stop-${time.trip_id}">
                    <div class="detail-route-name"><a href="${OJPP_BASE}/trips/${time.trip_id}" target="_blank">${time.route_name}</a>
                    <br><small>${time.operator.name}</small> ${time.vehicle ? `<small onclick="createVehicleSidebarPass(${time.vehicle.id}, ${time.trip_id})" class="bean bean-${time.operator.name ? 'ijpp' : ''}">📡 ${time.vehicle.plate ? time.vehicle.plate : time.vehicle.id}
                    ${time.vehicle.model ? time.vehicle.model.low_floor ? "♿" : "" : ""}</small>` : ""}</div>
                    <div class="detail-stop-time"> ${time.time_arrival != null ? time.time_arrival.substring(0, 5) : '-'}
                    <br>${time.time_departure != null && time.time_departure != '00:00' ? time.time_departure.substring(0, 5) : '-'}</div>
                </div>`;
				}
				$_details += "</td></tr>";
			} catch (e) {
				console.error(e);
				$_details = "";
			}

			document.getElementById("stop-arrivals-" + stop.id).innerHTML = `
				<table class="sidebar-table" >
					<tr>
						<th>${VOCABULARY.arrivals_to_stop}</th>
					</tr>
				</table>
				${$_details}
			`
		}
	})(),
	scbikes: new (class scbikes extends RideshareProvider {
		id = "scbikes";
		group = "kolesa_ostali";
		endpoint = "/scbikes/stations";
		vehiclesEndpoint = "/scbikes/vehicles";
		name = "SCBikes";
		getVehicleName(vehicle) {
			return vehicle.title || vehicle.model.replace('E_', '');
		}
		async make_layer() {
			let layer = await super.make_layer();
			layer.layout['icon-image'] = [
				"coalesce",
				["image", ['concat',
					'scbikes_',
					['get', 'system']
				]],
				["image", this.id],
			];

			return layer;
		}
	})(),
	micikel: new (class micikel extends RideshareProvider {
		id = "micikel";
		group = "kolesa_ostali";
		endpoint = "/micikel/stations";
		name = "micikel";
	})(),
	zapeljime: new (class zapeljime extends FloatingRideshareProvider {
		id = "zapeljime";
		group = "kolesa_ostali";
		endpoint = "/zapeljime/vehicles";
		name = "ZapeljiMe";
		async getPopup(feature) {
			let vehicle = feature.properties;
			return html`
				<strong>${vehicle.title}</strong>
			`;
		}
		getPanel = undefined;
	})(),
	urbanomati: new (class urbanomati extends Provider {
		id = "urbanomati";
		group = "urbanomati";
		endpoint = "/urbanaplus/kiosks";
		name = "urbanomati";
		async fetch_geojson() {
			let r = await fetch(this.api_url + this.endpoint);
			let json = await r.json();
			let geojson = geojson_convert(json, {
				id: "id",
				lat: "lat",
				lng: "lng",
			});
			return geojson;
		}
	})(),
};

const PROVIDER_ORDER = [
	'ojpp_postaje',
	'lpp_postaje',
	'marprom_postaje',
	'arriva_koper_postaje',
	'nomago_celje_postaje',
	'urbanomati',
	'nomago_bikes',
	'bicikelj',
	'mbajk',
	'micikel',
	'scbikes',
	'zapeljime',
	'sharengo',
	'greengo',
	'avant2go',
	'lpp_lokacije',
	'marprom_lokacije',
	'sz_lokacije',
	'ijpp_lokacije',
]

let SECTIONS = [
	{
		title: "Izposoja koles",
		icon: "bike",
		provider_groups: [
			{
				name: "europlakat",
			},
			{
				name: "nomago_bikes",
			},
			{
				name: "kolesa_ostali",
			},
		],
	},
	{
		title: "Javni prevoz",
		icons: "bus",
		provider_groups: [
			{
				name: "mestni_postaje",
			},
			{
				name: "sz_postaje",
			},
			{
				name: "ojpp_postaje",
			},
		],
	},
	{
		title: "Izposoja avtomobilov",
		icons: "car",
		provider_groups: [
			{
				name: "avant2go",
			},
			{
				name: "sharengo",
			},
			{
				name: "greengo",
			},
		],
	},
	{
		title: "Lokacije vozil",
		icons: "gps",
		provider_groups: [
			{
				name: "mestni_lokacije",
			},
			{
				name: "ijpp_lokacije",
			},
			{
				name: "sz_lokacije",
			},
		],
	},
	{
		title: "Ostalo",
		icons: "",
		provider_groups: [
			{
				name: "urbanomati",
			},
		],
	},
];

async function groupSetVisible(group_name, visible) {
	let providers = Object.values(PROVIDERS).filter(p => p.group === group_name);
	let $el = document.querySelector(`.provider-group-toggle[data-provider-group="${group_name}"]`);

	if (visible === undefined)
		visible = !$el.classList.contains("active");

	$el.classList.add("loading");

	let groupVisible = false;

	for (let provider of providers) {
		let res = await provider.setVisible(visible);
		if (res === true)
			groupVisible = true;
	}

	$el.classList.remove("loading");
	$el.classList.toggle("active", groupVisible);

	console.debug(group_name, providers, groupVisible)

	let hidden_groups = JSON.parse(localStorage.getItem('hidden_groups') || "[]");

	if (hidden_groups.includes(group_name) && groupVisible)
		hidden_groups.remove(group_name);
	else if (!hidden_groups.includes(group_name) && !groupVisible)
		hidden_groups.push(group_name);

	localStorage.setItem('hidden_groups', JSON.stringify(hidden_groups));

}

async function load_all() {
	let hidden_groups = JSON.parse(localStorage.getItem("hidden_groups"));
	if (hidden_groups === null) {
		hidden_groups = [];
		localStorage.setItem("hidden_groups", "[]");
	}

	for (let provider of PROVIDER_ORDER.map(id => PROVIDERS[id])) {
		await provider.init();
		// provider.setVisible(!hidden_groups.includes(provider.id));
		if (provider.refreshTime > 0)
			setInterval(async () => {
				if (!document.hidden)
					await provider.refresh()
			}, provider.refreshTime * 1000);
	}

	for (let section of SECTIONS)
		for (let provider_group of section.provider_groups)
			if (!hidden_groups.includes(provider_group.name))
				await groupSetVisible(provider_group.name, true)
	return null
}
